<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>bestes</title>
    <link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet">
</head>
<body>
    <header>

        <h1>LOUP</h1>

    </header>

    <?php
$taille = get_field('taille');
$image = get_field('image');
$mange = get_field('mange');

 if ($image) : ?>
 <h2><?php the_title();?> </h2>
 <p> <?php echo the_content(); ?></p>
 
    <img src="<?php echo $image; ?>" alt="<?php the_title();  ?>" alt="animals">
<?php endif; ?>

<?php if ($mange) : ?>
        <ul>
            <?php foreach ($mange as $post) : ?>

                <?php setup_postdata($post); ?>


                <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
            <?php endforeach; ?>

            <?php wp_reset_postdata(); ?>
        </ul>
    <?php endif; ?>
    

    
</body>
</html>